package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody User user) {
        userService.add(user);
        return ResponseEntity.ok("User successfully added");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody User user) {
        userService.update(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody User user) {
        userService.delete(user);
        return ResponseEntity.ok("User successfully deleted");
    }
}
